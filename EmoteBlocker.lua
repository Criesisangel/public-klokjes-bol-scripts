--[[
    Emote Blocker 0.1 by Klokje
    ================================================================================
    
    Info:
    Fully customize Emote Blocker. 


    Change log:
    0.1  
        -  Initial release 
       
]]
-- Code -----------------------------------------------------------------------------
function OnLoad()
    PrintChat(" >> Emote Blocker 0.1 by Klokje")  

    EmoteConfig = scriptConfig("Emote Blocker", "EmoteBlocker") 
    EmoteConfig:addParam("Block", "Block all", SCRIPT_PARAM_ONOFF, false)
    EmoteConfig:addSubMenu("Ally",tostring(myHero.team))
    EmoteConfig:addSubMenu("Enemy", tostring(TEAM_ENEMY))
	
    EmoteConfig[tostring(myHero.team)]:addParam("Block", "Block all allys", SCRIPT_PARAM_ONOFF, false)
    EmoteConfig[tostring(TEAM_ENEMY)]:addParam("Block", "Block all enemys", SCRIPT_PARAM_ONOFF, false)


    for i = 1, heroManager.iCount do
        local hero = heroManager:GetHero(i)
        local team = tostring(hero.team) 

        EmoteConfig[team]:addSubMenu(hero.charName, hero.charName)
        EmoteConfig[team][hero.charName]:addParam("Block", "Block all", SCRIPT_PARAM_ONOFF, false)
        EmoteConfig[team][hero.charName]:addParam("Dance", "Block Dance", SCRIPT_PARAM_ONOFF, false)
        EmoteConfig[team][hero.charName]:addParam("Taunt", "Block Taunt", SCRIPT_PARAM_ONOFF, false)
        EmoteConfig[team][hero.charName]:addParam("Laugh", "Block Laugh", SCRIPT_PARAM_ONOFF, false)
        EmoteConfig[team][hero.charName]:addParam("Joke", "Block Joke", SCRIPT_PARAM_ONOFF, false)
    end
end 

function OnEmote(unit, type)
	if EmoteConfig.Block then return false end 

	if EmoteConfig[tostring(unit.team)].Block then return false end 

	if EmoteConfig[tostring(unit.team)][unit.charName] then 
		if EmoteConfig[tostring(unit.team)][unit.charName].Block then return false end 
		if EmoteConfig[tostring(unit.team)][unit.charName].Dance and type == EMOTE_DANCE then return false end 
		if EmoteConfig[tostring(unit.team)][unit.charName].Taunt and type == EMOTE_TAUNT then return false end 
		if EmoteConfig[tostring(unit.team)][unit.charName].Laugh and type == EMOTE_LAUGH then return false end 
		if EmoteConfig[tostring(unit.team)][unit.charName].Joke and type == EMOTE_JOKE then return false end 
	end 
end 